export default {
    "offre": [
        {
            "id": 1,
            "titre": "Développeur Web",
            "lieu": "Paris",
            "contrat": "CDI",
            "categorie": "Commerce",
            "date_publication": "2023-06-28"
        },
        {
            "id": 2,
            "titre": "Assistant Administratif",
            "lieu": "Lyon",
            "contrat": "CDD",
            "categorie": "Fonction Support",
            "date_publication": "2023-06-29"
        },
        {
            "id": 3,
            "titre": "Ingénieur Logiciel",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-06-30"
        },
        {
            "id": 4,
            "titre": "Responsable Marketing",
            "lieu": "Paris",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-06-30"
        },
        {
            "id": 5,
            "titre": "Chef de Projet",
            "lieu": "Marseille",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-01"
        },
        {
            "id": 6,
            "titre": "Technicien Support Informatique",
            "lieu": "Lyon",
            "contrat": "CDD",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-02"
        },
        {
            "id": 7,
            "titre": "Graphiste",
            "lieu": "Paris",
            "contrat": "Freelance",
            "categorie": "Consulting",
            "date_publication": "2023-07-02"
        },
        {
            "id": 8,
            "titre": "Analyste Financier",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-07-03"
        },
        {
            "id": 9,
            "titre": "Développeur Mobile",
            "lieu": "Lyon",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-03"
        },
        {
            "id": 10,
            "titre": "Assistant Ressources Humaines",
            "lieu": "Paris",
            "contrat": "CDD",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 11,
            "titre": "Ingénieur Système",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-07-04"
        },
        {
            "id": 12,
            "titre": "Commercial",
            "lieu": "Marseille",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 13,
            "titre": "Développeur Front-End",
            "lieu": "Paris",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 14,
            "titre": "Chef de Projet Marketing",
            "lieu": "Lyon",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 15,
            "titre": "Assistant Comptable",
            "lieu": "Toulouse",
            "contrat": "CDD",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 16,
            "titre": "Designer UI/UX",
            "lieu": "Paris",
            "contrat": "Freelance",
            "categorie": "Commerce",
            "date_publication": "2023-07-04"
        },
        {
            "id": 17,
            "titre": "Analyste Marketing",
            "lieu": "Lyon",
            "contrat": "CDI",
            "categorie": "Fonction Support",
            "date_publication": "2023-07-04"
        },
        {
            "id": 18,
            "titre": "Développeur Backend",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Commerce",
            "date_publication": "2023-07-04"
        },
        {
            "id": 19,
            "titre": "Assistant Juridique",
            "lieu": "Paris",
            "contrat": "CDD",
            "categorie": "Consulting",
            "date_publication": "2023-07-04"
        },
        {
            "id": 20,
            "titre": "Chef de Projet IT",
            "lieu": "Lyon",
            "contrat": "CDI",
            "categorie": "Commerce",
            "date_publication": "2023-07-04"
        },
        {
            "id": 21,
            "titre": "Architecte Logiciel",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-07-04"
        },
        {
            "id": 22,
            "titre": "Assistant Marketing",
            "lieu": "Paris",
            "contrat": "CDD",
            "categorie": "Commerce",
            "date_publication": "2023-07-04"
        },
        {
            "id": 23,
            "titre": "Développeur Full Stack",
            "lieu": "Lyon",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-07-04"
        },
        {
            "id": 24,
            "titre": "Gestionnaire de Projets",
            "lieu": "Toulouse",
            "contrat": "CDI",
            "categorie": "Commerce",
            "date_publication": "2023-07-04"
        },
        {
            "id": 25,
            "titre": "Assistant Commercial",
            "lieu": "Paris",
            "contrat": "CDI",
            "categorie": "Consulting",
            "date_publication": "2023-07-04"
        }
    ]
}